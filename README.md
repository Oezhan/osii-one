![Static Badge](https://img.shields.io/badge/DIN%20SPEC%203105-pending-blue?logo=opensourcehardware)

<!--- make more badges here: https://shields.io/badges -->

# OSI² ONE: An Open Source low-field MRI scanner

OSI² ONE is an open source MRI scanner aiming to make diagnostic imaging more accessible around the globe.
It has been developed by the [Open Source Imaging Initiative (OSI²)](https://www.opensourceimaging.org/).

The very first prototype has been presented at the ISMRM workshop on low-field MRI (17th of March 2022)

**System specifications:**

- `$B_0=50mT$`
- `$\o_Magnet=30cm$`
	- so it's suitable for head & limbs
- total material costs ≈ 20k€

TODO
SOME IMAGE

## Replications

We have knowledge about the following rebuilds:

- Leiden University Medical Center (LUMC), Netherlands
- Mbarara University of Science & Technology (MUST), Uganda
- Physikalisch-Technische Bundesanstalt (PTB), Berlin, Germany

If you have built your own OSI² ONE – or want to or know of someone who did – please feel free to drop us an [issue](https://gitlab.com/osii-one/mri-scanners/osii-one/-/issues/new) so we can add it to the list and/or get in contact with you :)

## Modules/Dependencies

### Hardware

| (image) | Module                        | Version                                                                                                          |
|---------|-------------------------------|------------------------------------------------------------------------------------------------------------------|
|         | 30cm Halbach Magnet           | [lumc-30cm-v2](https://gitlab.com/osii-one/magnet/30cm-halbach-magnet/-/tree/lumc-30cm-v2)                       |
|         | Gradient Power Amplifier ±15A | [xxx](https://gitlab.com/osii/gradient-system/gradient-power-amplifier/tu-delft-15a/tu-delft-ga2)                |
|         | 1kW Pulsed RF Power Amplifier | [xxx](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa)                                        |
|         | XYZ Gradient Coils            | [xxx](https://gitlab.com/osii/gradient-system/gradient-coils/xyz-gradients/xyz-gradient-inserts-30cm)            |
|         | RF Coils                      | [see here](https://gitlab.com/osii/rf-system/rf-coils)                                                           |
|         | TR Switches and Preamps       | [commit 5479274](https://github.com/opensourceimaging/tr-switch/commit/5479274b12ffe68bc166b205807871cc23afeb75) |

Furthermore:

TODO
- redpitaya (which?)
- https://github.com/OpenMRI/ocra

### Software

- ScanHub: https://github.com/brain-link/scanhub
- ScanHub-UI: https://github.com/brain-link/scanhub-ui
- CoilGen: https://github.com/Philipp-MR/CoilGen
	- alternatively: pycoilgen:
		- https://test.pypi.org/project/pycoilgen/
		- https://github.com/kev-m/pyCoilGen

## Build Instructions

TODO

link to MD table (separate document)

## Contribute

Currently, our contribution management relies on personal connections and reviews.
If you want to get in touch with us and join the development or deployment of accessible open source MRI tech, we'd love to hereby invite you over to the [OSI² Matrix Space](https://matrix.to/#/#osii:matrix.org) 💜.

## Usage

TODO

Link to operating instructions

## Support

There's yet no commercial support in place for OSI² ONE.
However, we're happy to answer all your burning questions :)
Just drop us an [issue](https://gitlab.com/osii-one/mri-scanners/osii-one/-/issues/new) or ask us on [Matrix](https://matrix.to/#/#osii:matrix.org).

## License and Liability

This project is licensed under [CERN Open Hardware Licence Version 2 - Weakly Reciprocal](LICENSE).
Licenses of the individual modules of OSI² ONE apply accordingly.

Please check also the [DISCLAIMER.pdf](DISCLAIMER.pdf) for more details.
